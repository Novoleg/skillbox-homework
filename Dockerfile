FROM golang:latest as builder

RUN mkdir /app
WORKDIR /app 

COPY ./go.mod /app
COPY ./go.sum /app
RUN go mod download

COPY . /app

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /app/pig


FROM alpine

RUN mkdir /app
WORKDIR /app

COPY --from=builder /app/pig .
COPY --from=builder /app/resources/index.html .
EXPOSE 8000
CMD ["./pig"]

